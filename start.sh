geth --datadir ../wallets/ \
    --http --http.port 30313 --http.rpcprefix '/' --http.api "eth,net,personal,web3,debug" \
    --ws --ws.port 30323 --ws.api "eth,net,personal,web3,debug" \
    --allow-insecure-unlock --networkid 11111 --nodiscover \
    --mine --miner.threads 1 --miner.etherbase 0x6b574ff37fc0bec5e7938d5528ca8e83350ba9fe \
    js mine_transactions_only.js 
#geth --http --http.port 30313  \ #--http.rpcprefix '/' # serve rpc on all paths
 #   --datadir ../wallets/ --networkid 11111 --nodiscover \
  #  --mine --miner.threads 1 \ 
   # js mine_transactions_only.js # mine only when there are transactions
    #--http.rpcprefix '/' # serve rpc on all paths
    